import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaitemsComponent } from './listaitems.component';

describe('ListaitemsComponent', () => {
  let component: ListaitemsComponent;
  let fixture: ComponentFixture<ListaitemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaitemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaitemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
