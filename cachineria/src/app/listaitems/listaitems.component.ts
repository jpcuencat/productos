import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listaitems',
  templateUrl: './listaitems.component.html',
  styleUrls: ['./listaitems.component.css']
})
export class ListaitemsComponent implements OnInit {
  listado: string[];
  constructor() { 
    this.listado = ['Protectores Faciales','Macarillas Antifluidos','Termotros']
  }

  ngOnInit(): void {
  }

}
