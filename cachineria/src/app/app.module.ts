import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ItemsComponent } from './items/items.component';
import { ListaitemsComponent } from './listaitems/listaitems.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemsComponent,
    ListaitemsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
